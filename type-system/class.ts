interface IInterface1 {
    soma(a, b): number;
}
interface IInterface2 {}

class NomeDaClasse implements IInterface1, IInterface2 {

    private atributo1: number;
    public atributo2: boolean;
    protected atributo3: Object;

    constructor(parametros: boolean) {
        this.atributo2 = parametros;
    }

    soma(a, b) {
        return a + b;
    }

    imprimir(message: string): void {
        if(this.atributo2) {
            console.log(message);
        }
    }
    
}

const novo = new NomeDaClasse(true);
novo.imprimir('Hello, World!!!');

export default NomeDaClasse;