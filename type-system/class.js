"use strict";
exports.__esModule = true;
var NomeDaClasse = /** @class */ (function () {
    function NomeDaClasse(parametros) {
        this.atributo2 = parametros;
    }
    NomeDaClasse.prototype.imprimir = function (message) {
        if (this.atributo2) {
            console.log(message);
        }
    };
    return NomeDaClasse;
}());
var novo = new NomeDaClasse(true);
novo.imprimir('Hello, World!!!');
exports["default"] = NomeDaClasse;
