"use strict";
exports.__esModule = true;
var UserRoutes = /** @class */ (function () {
    function UserRoutes() {
    }
    UserRoutes.prototype.index = function (req, Request, res) {
    };
    UserRoutes.prototype.create = function (req, Request, res) {
    };
    UserRoutes.prototype.findOne = function (req, Request, res) {
    };
    UserRoutes.prototype.update = function (req, Request, res) {
    };
    UserRoutes.prototype.destroy = function (req, Request, res) {
    };
    return UserRoutes;
}());
exports["default"] = UserRoutes;
