module.exports = {
    env: 'test',
    db: 'ts-api-test',
    dialect: 'postgres',
    username: 'postgres',
    password: 'root',
    host: 'localhost',
    serverPort: 3000,
    pgPort: 5432,
    dbURL: 'postgres://postgres:root@localhost:5432/ts-api-test',
    secret: '$54cr5c'
}