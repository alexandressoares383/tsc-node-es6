let pessoa = {
    "nome": "Pessoa",
    "sobrenome": "Qualquer"
}

let nome, sobrenome;

({ nome, sobrenome } = pessoa);

console.log(nome, sobrenome);